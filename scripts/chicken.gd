extends CharacterBody2D

enum CHICKEN_STATE { SLEEP, IDLE, WALK }
@export var move_speed : float = 60
@export var idle_time : float = 5
@export var walk_time : float = 2
@export var sleep_time : float = 4

@onready var sprite = $AnimatedSprite2D
@onready var timer = $Timer
@onready var cluck = $Cluck

var move_direction: Vector2 = Vector2.ZERO
var current_state: CHICKEN_STATE = CHICKEN_STATE.IDLE

func _ready():
	select_new_direction()
	pick_new_state()
	

func _physics_process(_delta):
	if(current_state == CHICKEN_STATE.WALK):
		velocity = move_direction * move_speed
		move_and_slide()

#Random movement -1, 0, 1 X or y
func select_new_direction():
	move_direction = Vector2(
		randi_range(-1,1),
		randi_range(-1,1)
	)
	if(move_direction.x < 0):
		sprite.flip_h = false
	elif(move_direction.x > 0):
		sprite.flip_h = true
		
	
#change Idle to Walk
func pick_new_state():
	var initiative = randi_range(0,5)
	if(current_state == CHICKEN_STATE.SLEEP):
		if (initiative >= 3):
			cluck.play()
			sprite.play("idle")
			current_state = CHICKEN_STATE.IDLE
			select_new_direction()
			timer.start(idle_time)
		else:
			sprite.play("sleep")
			timer.start(sleep_time)
	
	elif(current_state == CHICKEN_STATE.IDLE):
		if (initiative >=4):			
			sprite.play("run")
			current_state = CHICKEN_STATE.WALK
			select_new_direction()
			timer.start(walk_time)
		elif (initiative <= 2):
			sprite.play("sleep")
			current_state = CHICKEN_STATE.SLEEP
			timer.start(sleep_time)
		else:
			sprite.play("idle")
			select_new_direction()
			timer.start(idle_time)

	elif(current_state == CHICKEN_STATE.WALK):
		if (initiative >=4):
			sprite.play("run")
			select_new_direction()
			timer.start(walk_time)
		else:
			sprite.play("idle")
			current_state = CHICKEN_STATE.IDLE
			select_new_direction()
			timer.start(idle_time)


func _on_timer_timeout():
	pick_new_state()
