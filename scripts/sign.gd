@tool
extends StaticBody2D

@onready var sprite = $Sprite2D
@export_enum("Rectangle", "Right", "Left") var variant = 0:
	set(new):
		if sprite:			
			sprite.frame = new 
		variant = new

@onready var label = $TextLabel
@export var sign_text :String = "":
	set(new_text):
		if label:
			label.text = new_text
		sign_text = new_text

@export var preview :bool = true:
	set(new_preview):
		if label:
			label.visible = new_preview
		preview = new_preview

func _ready():
	sprite.frame = variant
	label.text = sign_text
	label.visible = false
	#print("Sign Ready Boss: ",sign_text)

func _on_trigger_zone_body_entered(_body):
	#print ("body entered")
	label.visible = true

func _on_trigger_zone_body_exited(_body):
	#print ("body exited")
	label.visible = false
