# Tamago

Tamago means 'egg' in Japanese, or so I've been told. 

## A game project

Learning to code in Godot 4

## Author

dracoling

## License

This repository and all code herein are by the authors and contributors to this git repository and licensed CC BY-SA 4.0 with the exception of assets listed in the file assets/sources.txt which may be under other licenses and by other authors.