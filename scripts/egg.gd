extends Area2D

@onready var game_manager = get_tree().get_first_node_in_group("GameManager")

func _on_body_entered(_body):
	game_manager.pickup("egg")
	queue_free()
